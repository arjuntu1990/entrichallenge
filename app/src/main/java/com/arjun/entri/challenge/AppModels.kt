package com.arjun.entri.challenge

data class CommonResponse<T>(val ok: Boolean, val data: T)
data class AuthResponse(
    val access_token: String,
    val scope: String
)

data class ChannelHistory(
    val ok: Boolean,
    val messages: List<MessageData>,
    val has_more: Boolean
)

data class MessageData(
    val type: String,
    val text: String,
    val username: String?,
    val user: String?,
    val ts:Float,
    var channel:String?
)

data class SendMessage(
    val ok: Boolean,
    val message: MessageData,
    val ts:String,
    val channel:String
)