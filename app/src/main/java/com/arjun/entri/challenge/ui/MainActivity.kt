package com.arjun.entri.challenge.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.arjun.entri.challenge.MessageData
import com.arjun.entri.challenge.network.NetworkBuilder
import com.arjun.entri.challenge.ui.common.ApiResult
import com.arjun.entri.challenge.utils.CHANNEL_ID_ANDROID
import com.arjun.entri.challenge.utils.CHANNEL_ID_GENERAL
import kotlinx.android.synthetic.main.activity_main.*
import com.arjun.entri.challenge.R
import androidx.appcompat.app.AlertDialog


class MainActivity : AppCompatActivity() {

    private val mainVM:MainVM by lazy {
        getViewModel { MainVM(MainUsecase(MainRepository(NetworkBuilder.getApi()))) }
    }
    private val combinedHistory = ArrayList<MessageData>()
    private lateinit var chatHistoryAdap: ChatHistoryAdap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        getAccess()
    }

    private fun initViews() {
        chatHistoryAdap = ChatHistoryAdap(combinedHistory)
        rv_chat_history.layoutManager = LinearLayoutManager(this)
        rv_chat_history.adapter = chatHistoryAdap

        val selectChannelDialog = getSelectChanelDialog()

        btn_send.setOnClickListener {
            selectChannelDialog.show()
        }
    }

    private fun getSelectChanelDialog() : AlertDialog.Builder {
        return AlertDialog.Builder(this)
            .setTitle("Send Message")
            .setMessage("Select channel to send message")
            .setCancelable(true)
            .setPositiveButton("General",
                { dialog, which ->
                    mainVM.sendMessage(CHANNEL_ID_GENERAL, et_new_chat.text.toString())
                })
            .setNegativeButton("Android",
                { dialog, which ->
                    mainVM.sendMessage(CHANNEL_ID_ANDROID, et_new_chat.text.toString())
                })
    }

    private fun getAccess() {
        mainVM.getChannelHistory()
        observeHistory()
    }

    private fun observeHistory() {
        mainVM.historyHolder.observe(this, Observer {
            when(it) {
//                is ApiResult.Loading ->
                is ApiResult.Success -> {
                    combinedHistory.clear()
                    combinedHistory.addAll(it.data)
                    chatHistoryAdap.notifyDataSetChanged()
                    rv_chat_history.scrollToPosition(it.data.size - 1)
                }
            }
        })
    }
}
