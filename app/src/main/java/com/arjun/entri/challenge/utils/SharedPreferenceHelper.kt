package com.arjun.entri.challenge.utils

import android.content.Context
import android.preference.PreferenceManager

object SharedPreferenceHelper {

    fun getString(mContext: Context?, key: String, _default: String): String {
        val preferences = PreferenceManager
                .getDefaultSharedPreferences(mContext)
        return preferences.getString(key, _default)
    }

    fun putString(mContext: Context?, key: String, value: String) {
        val preferences = PreferenceManager
                .getDefaultSharedPreferences(mContext)
        val edit = preferences.edit()
        edit.putString(key, value)
        edit.apply()

    }
}