package com.arjun.entri.challenge.network

import com.arjun.entri.challenge.ChannelHistory
import com.arjun.entri.challenge.SendMessage
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("channels.history")
    fun getChannelHistory(@Query("token") token: String,
                @Query("channel") channel: String): Single<ChannelHistory>

    @GET("chat.postMessage")
    fun sendMessage(@Query("token") token: String,
                          @Query("channel") channel: String,
                          @Query("text") text: String): Single<SendMessage>
}