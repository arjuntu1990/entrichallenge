package com.arjun.entri.challenge.network

import com.arjun.entri.challenge.MainApplication
import com.arjun.entri.challenge.utils.API_TOKEN
import com.arjun.entri.challenge.utils.SharedPreferenceHelper
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class NetworkBuilder {

    companion object {
        fun getApi(): ApiService {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()

            httpClient.readTimeout(2, TimeUnit.MINUTES).connectTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)

//            httpClient.addInterceptor(HeaderInterceptor())

            httpClient.addInterceptor(logging)

            val retrofit = Retrofit.Builder().baseUrl("https://slack.com/api/").
                    addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient.build()).build()


            return retrofit.create(ApiService::class.java)
        }
    }

    private class HeaderInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            var request = chain.request()

            try {
                request = request.newBuilder()
                        .addHeader("api_token", SharedPreferenceHelper.getString(MainApplication.instance, API_TOKEN, ""))
                        .build()
            } catch (e: Exception) {
            }

            return chain.proceed(request)
        }
    }
}