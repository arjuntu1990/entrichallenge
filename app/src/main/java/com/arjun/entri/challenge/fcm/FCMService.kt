package com.arjun.entri.challenge.fcm

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.arjun.entri.challenge.R
import com.arjun.entri.challenge.utils.SharedPreferenceHelper
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*

class FCMService : FirebaseMessagingService() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)
        p0?.data?.run {
                if (containsKey("name") && containsKey("message")) {
                    LocalBroadcastManager.getInstance(this@FCMService)
                            .sendBroadcast(Intent(ACTION_CHAT_RECIEVED).apply {
                                putExtra("channel", get("id") as String)
                                putExtra("message", get("name") as String)
                                putExtra("user", get("image")?:"")
                            })

                    PushNotification(getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).show(this@FCMService,this)
                    //makeNotification(get("name") as String, get("message") as String, )
                }

        }


    }

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        SharedPreferenceHelper.putString(this, "fcm_token", p0 ?: "")
    }

    private fun makeNotification(messageBody: String, title: String, intent: Intent) {
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, "chat")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(Calendar.getInstance().timeInMillis.toInt(), notificationBuilder.build())
    }

    companion object {
        const val RECIEVED_MSG = "msg"
        const val ACTION_CHAT_RECIEVED = "chat_recieved"
    }
}