package com.arjun.entri.challenge.ui

import com.arjun.entri.challenge.AuthResponse
import com.arjun.entri.challenge.ChannelHistory
import com.arjun.entri.challenge.SendMessage
import com.arjun.entri.challenge.network.ApiService
import io.reactivex.Single

class MainRepository(private val service: ApiService) {
    fun getChannelHistory(token: String,
                   channel: String): Single<ChannelHistory> {
        return service.getChannelHistory(token, channel)
    }

    fun getChannelHistory(token: String,
                          channel: String, message:String): Single<SendMessage> {
        return service.sendMessage(token, channel, message)
    }
}