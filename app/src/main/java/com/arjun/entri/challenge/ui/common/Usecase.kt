package com.arjun.entri.challenge.ui.common

import io.reactivex.Single

abstract class Usecase<in P, R> {
    abstract fun execute(parameters: P): Single<R>
}