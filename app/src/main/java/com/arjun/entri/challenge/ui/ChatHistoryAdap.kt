package com.arjun.entri.challenge.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arjun.entri.challenge.MessageData
import com.arjun.entri.challenge.R
import kotlinx.android.synthetic.main.item_chat.view.*

class ChatHistoryAdap(val chats:List<MessageData>) : RecyclerView.Adapter<ChatHistoryAdap.ChatVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatVH {
        return ChatVH(LayoutInflater.from(parent.context).inflate(R.layout.item_chat, parent, false))
    }

    override fun getItemCount(): Int {
        return chats.size
    }

    override fun onBindViewHolder(holder: ChatVH, position: Int) {
        chats.get(position).run {
            holder.itemView.tv_user.text = user ?: username
            holder.itemView.tv_message.text = text
            holder.itemView.tv_channel_name.text = channel
        }
    }

    class ChatVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}