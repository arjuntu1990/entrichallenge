package com.arjun.entri.challenge.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.arjun.entri.challenge.CommonResponse
import com.arjun.entri.challenge.ui.common.BaseViewModelFactory
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers


fun <T> Single<T>.applySchedulers(): Single<T> {

    return subscribeOn(Schedulers.io()).map {
        return@map it
    }

}

inline fun <reified T : ViewModel> Fragment.getViewModel(noinline creator: (() -> T)? = null): T {
    return if (creator == null)
        ViewModelProviders.of(this).get(T::class.java)
    else
        ViewModelProviders.of(this, BaseViewModelFactory(creator)).get(T::class.java)
}

inline fun <reified T : ViewModel> FragmentActivity.getViewModel(noinline creator: (() -> T)? = null): T {
    return if (creator == null)
        ViewModelProviders.of(this).get(T::class.java)
    else
        ViewModelProviders.of(this, BaseViewModelFactory(creator)).get(T::class.java)
}