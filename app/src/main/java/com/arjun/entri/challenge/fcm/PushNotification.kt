package com.arjun.entri.challenge.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.arjun.entri.challenge.R
import com.arjun.entri.challenge.ui.MainActivity

class PushNotification(private val notificationManager: NotificationManager) {

    fun show(context: Context, data: Map<String, String>) {
        val id = data["id"]?.hashCode() ?: 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!notificationChannelExists("chat")) {
                createChannel(context,"chat","Chats")
            }
        }

        val dataIntent = Intent(context,
                MainActivity::class.java).apply {
            putExtra("channel", data.get("id") as String)
            putExtra("message", data.get("name") as String)
            putExtra("user", data.get("image")?:"")
        }

        val pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, dataIntent,
                PendingIntent.FLAG_ONE_SHOT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context, "chat")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.ic_launcher_foreground))
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(data.get("name") as String + "has send you a message")
                .setAutoCancel(true)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(data.get("message") as String))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        notificationManager.notify(id,notificationBuilder.build())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel(context: Context, channelId: String,channelTitle : String) {
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val notificationChannel = NotificationChannel(channelId, channelTitle, importance)
        notificationChannel.setShowBadge(true)
        notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationManager.createNotificationChannel(notificationChannel)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun notificationChannelExists(channelId: String): Boolean =
            notificationManager.getNotificationChannel(channelId) != null
}