package com.arjun.entri.challenge.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.arjun.entri.challenge.ChannelHistory
import com.arjun.entri.challenge.MessageData
import com.arjun.entri.challenge.SendMessage
import com.arjun.entri.challenge.ui.common.ApiResult
import com.arjun.entri.challenge.ui.common.BaseVM
import com.arjun.entri.challenge.utils.CHANNEL_ID_ANDROID
import com.arjun.entri.challenge.utils.CHANNEL_ID_GENERAL
import io.reactivex.android.schedulers.AndroidSchedulers

class MainVM(private val usecase: MainUsecase) : BaseVM() {
    private val _historyHolder = MutableLiveData<ApiResult<List<MessageData>>>()
    val historyHolder: LiveData<ApiResult<List<MessageData>>> = _historyHolder
    val chatHistory = ArrayList<MessageData>()

    private val _sendMessageHolder = MutableLiveData<ApiResult<SendMessage>>()
    val sendMessageHolder: LiveData<ApiResult<SendMessage>> = _sendMessageHolder

    fun getChannelHistory() {
        compositeDisposable.add(usecase.execute(CHANNEL_ID_GENERAL)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _historyHolder.postValue(ApiResult.Loading(true))
            }
            .subscribe({
//                _historyHolder.postValue(ApiResult.Success(it))
                chatHistory.addAll(it.messages)
                getAndroidChannelHistory()
            }, {
                _historyHolder.postValue(ApiResult.Error(it))
            }))
    }

    private fun getAndroidChannelHistory() {
        compositeDisposable.add(usecase.execute(CHANNEL_ID_ANDROID)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _historyHolder.postValue(ApiResult.Loading(true))
            }
            .subscribe({
                chatHistory.addAll(it.messages)
//                channelHistory.sortBy { it.ts }
                chatHistory.sortBy { it.ts }
                _historyHolder.postValue(ApiResult.Success(chatHistory))
            }, {
                _historyHolder.postValue(ApiResult.Error(it))
            }))
    }

    fun sendMessage(channel: String, message:String) {
        compositeDisposable.add(usecase.sendMessage(channel, message)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _sendMessageHolder.postValue(ApiResult.Loading(true))
            }
            .subscribe({
                chatHistory.add(it.message)
                _historyHolder.postValue(ApiResult.Success(chatHistory))
            }, {
            }))
    }
}