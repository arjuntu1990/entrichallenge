package com.arjun.entri.challenge.ui

import com.arjun.entri.challenge.ChannelHistory
import com.arjun.entri.challenge.SendMessage
import com.arjun.entri.challenge.ui.common.Usecase
import com.arjun.entri.challenge.utils.CHANNEL_ID_ANDROID
import com.arjun.entri.challenge.utils.TOKEN
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class MainUsecase(private val repository: MainRepository) : Usecase<String, ChannelHistory>() {
    override fun execute(parameters: String): Single<ChannelHistory> {
        val channel =  if (parameters.equals(CHANNEL_ID_ANDROID)) "Android" else "General"
        return repository.getChannelHistory(TOKEN, parameters).subscribeOn(Schedulers.io()).map {
        it.messages.map { it.channel = channel }
            it.messages.sortedBy { it.ts }
            return@map it
        }
    }

    fun sendMessage(channel: String, message: String): Single<SendMessage> {
        return repository.getChannelHistory(TOKEN, channel, message)
            .subscribeOn(Schedulers.io()).map {
                it.message.channel = if (channel.equals(CHANNEL_ID_ANDROID)) "Android" else "General"
                return@map it
            }
    }
}